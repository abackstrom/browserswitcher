//
//  BSAppDelegate.h
//  BrowserSwitcher
//
//  Created by Annika Backstrom on 2014-08-27.
//  Copyright (c) 2014 Annika Backstrom. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BSAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (readwrite, retain) IBOutlet NSMenu *menu;
@property (readwrite, retain) IBOutlet NSStatusItem *statusItem;

- (IBAction)menuAction:(id)sender;

@end
