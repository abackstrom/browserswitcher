//
//  BSAppDelegate.m
//  BrowserSwitcher
//
//  Created by Annika Backstrom on 2014-08-27.
//  Copyright (c) 2014 Annika Backstrom. All rights reserved.
//

#import "BSAppDelegate.h"

@implementation BSAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

- (void)awakeFromNib
{
    self.statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    self.statusItem.menu = self.menu;
    self.statusItem.title = @"Browser";
    self.statusItem.highlightMode = YES;

    NSArray *identifiers = [self getHandlerBundleIdentifiers];
    NSString *defaultHandler = [self getDefaultHandler];

    for (NSString *identifier in identifiers) {
        NSLog(@"Adding %@", identifier);
        NSMenuItem *menuItem = [[NSMenuItem alloc] initWithTitle:identifier
                                                          action:@selector(handleBrowserSelect)
                                                   keyEquivalent:@""];

        if ([identifier isEqual:defaultHandler])
        {
            menuItem.state = NSOnState;
        }

        [self.menu insertItem:menuItem atIndex:0];
    }
}

- (void)handleBrowserSelect
{
    NSLog(@"handleBrowserSelect");
}

- (NSArray *)getHandlerBundleIdentifiers
{
    NSMutableArray *identifiers = [[NSMutableArray alloc] init];

    CFArrayRef httpHandlers = LSCopyAllHandlersForURLScheme(CFSTR("https"));
    CFIndex handlerCount = CFArrayGetCount(httpHandlers);

    CFStringRef bundleIdentifier;

    for (int i = 0; i < handlerCount; i++) {
        bundleIdentifier = CFArrayGetValueAtIndex(httpHandlers, i);

        [identifiers addObject:[NSString stringWithString:(__bridge NSString *)bundleIdentifier]];

        CFRelease(bundleIdentifier);
    }

    return identifiers;
}

- (NSString *)getDefaultHandler
{
    CFStringRef httpHandler = LSCopyDefaultHandlerForURLScheme(CFSTR("https"));

    return (__bridge NSString *)httpHandler;
}

- (void)menuAction:(id)sender
{
    NSLog(@"Menu");
}

@end
