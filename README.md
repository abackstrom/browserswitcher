# BrowserSwitcher

This is an ~80% complete implementation of a menu bar item to switch the default
browser in Mac OS X. I got this far before finding
[Objectiv](http://nthloop.github.io/Objektiv/) and thought I'd post my code
anyway.

![screenshot](screenshot1.jpg)
