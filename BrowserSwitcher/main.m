//
//  main.m
//  BrowserSwitcher
//
//  Created by Annika Backstrom on 2014-08-27.
//  Copyright (c) 2014 Annika Backstrom. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
